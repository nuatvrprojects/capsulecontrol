const socket = require("socket.io")();
var oscLib = require("osc");
const port = 3001;
socket.listen(port, () => console.log(`Listening on port ${port}`));

var osc = new oscLib.UDPPort({
    localAddress: "192.168.1.111",
    localPort: 5555
});

osc.on("ready", function () {
});

osc.open();


function start() {
    console.log("starting game");
    osc.send({
        address: "/start"
    }, "192.168.1.255", 9000);
}

function restart() {
    console.log("restarting game");

    osc.send({
        address: "/restart"
    }, "192.168.1.255", 9000);}

function quit() {
    console.log("quitting game");

    osc.send({
        address: "/quit"
    }, "192.168.1.255", 9000);}

function restartPC() {
    console.log("restarting PC");

    osc.send({
        address: "/restartPC"
    }, "192.168.1.255", 9000);}

socket.on("connection", socket => {
    console.log("Client connected");
    socket.on("startgame", () => start());
    socket.on("restartgame", () => restart());
    socket.on("quitgame", () => quit());
    socket.on("restartPC", () => restartPC());

});