import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import { ButtonToolbar } from 'react-bootstrap';
import { Panel } from 'react-bootstrap';
import { ProgressBar } from 'react-bootstrap';

import io from "socket.io-client";

const wellStyles = { margin: '20px', font: '36px' };
const panelStyle = { margin: '100px' };

const socket = io("192.168.1.111:3001");

class App extends Component {
    constructor() {
        super();

        this.state = {
            endpoint: "http://127.0.0.1:4001",
            blah: "blah"
        };
    }


    componentDidMount() {
        console.log("component mounted");
    }

    handleButtonPress(buttonTitle) {
        console.log("Sending: " + buttonTitle);
        socket.emit(buttonTitle);
    }

    render() {
        console.log("RENDERING");
        return (
            <div className="App">
                <jumbotron>
                    <h1>Space Capsule Control Panel</h1>

                    <div className="btn">
                        <Button bsStyle="success" onClick={() => this.handleButtonPress("startgame")}>  Start Game</Button>
                        <Button bsStyle="warning" onClick={() => this.handleButtonPress("restartgame")}>    Restart Game</Button>
                        <Button bsStyle="danger" onClick={() => this.handleButtonPress("quitgame")}>    Quit Game</Button>
                        <Button bsStyle="primary" onClick={() => this.handleButtonPress("restartPC")}>  Restart all PCs</Button>


                    </div>
                </jumbotron>

                <alert bsStyle="warning">
                    <p><strong className="warningTxt">Holy guacamole!</strong> Best check yo self, you're not looking too
                    good.</p>
                </alert>

            </div>
        )
    }
}
export default App;
